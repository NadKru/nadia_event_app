class CreateEventJobPeriodAssociations < ActiveRecord::Migration
  def change
    create_table :event_job_period_associations do |t|
      t.integer :event_id
      t.integer :job_id
      t.integer :period_id
      t.integer :job_end

      t.timestamps null: false
    end
  end
end
